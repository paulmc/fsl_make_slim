FROM tensorflow/tensorflow:1.15.2-py3
MAINTAINER Paul McCarthy <pauldmccarthy@gmail.com>

ADD install_fsl.sh  /
ADD data            /usr/local/fsl/data/

ENV PATH          "/usr/local/fsl/bin:$PATH"
ENV FSLDIR        "/usr/local/fsl"
ENV FSLOUTPUTTYPE NIFTI_GZ

RUN bash /install_fsl.sh
