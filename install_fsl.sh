#!/bin/sh

set -e

export DEBIAN_FRONTEND=noninteractive

apt-get -y update
apt-get -y install wget file bc

wget https://repo.anaconda.com/miniconda/Miniconda3-py37_4.8.3-Linux-x86_64.sh

sh ./Miniconda3-py37_4.8.3-Linux-x86_64.sh -b -p /miniconda3/
rm ./Miniconda3-py37_4.8.3-Linux-x86_64.sh

CONDABASE=/miniconda3
CONDA=$CONDABASE/bin/conda
CONDARC=$CONDABASE/.condarc

$CONDA config --file $CONDARC --set    channel_priority strict
$CONDA config --file $CONDARC --add    channels         conda-forge
$CONDA config --file $CONDARC --append channels         https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/channel
$CONDA config --file $CONDARC --append channels         http://localhost:8000/

$CONDA create -y -p /usr/local/fsl python=3.7
source $CONDABASE/bin/activate /usr/local/fsl

# need newer fslpy to get low level path utils
pip install https://git.fmrib.ox.ac.uk/fsl/fslpy/-/archive/master/fslpy-master.tar.gz
conda install -y \
      scikit-learn \
      fsl-avwutils \
      fsl-flirt \
      fsl-fnirt \
      fsl-bet2 \
      fsl-fast4 \
      fsl-bianca \
      fsl-cluster \
      fsl-misc_scripts

ln -s /usr/local/fsl/bin/python /usr/local/fsl/bin/fslpython

rm -rf /usr/local/fsl/include
find /usr/local/fsl/lib/python3.7/site-packages -name "tests" | xargs rm -r

$CONDA clean -y -a -f -c $CONDABASE /usr/local/fsl/

apt-get -y clean
rm -rf /var/lib/apt/lists/*
